<?php
namespace App\HttpController;
use think\Db;
use App\HttpController\Base;
use App\WebSocket\Storage\Room;
class Member extends Base{
    /*
    * 获取在线用户
    * */
    function online(){
        $room_id =  $this->params['room_id'];
        if(empty($room_id)){
            return $this->writeJson(400,'','房间号不能为空');
        }
        $userList = Room::selectRoomAllUser($room_id);
        return $this->writeJson(200,$userList);
    }
    /*
    * 登录
    * */
    function login(){
        $mobile = $this->request()->getRequestParam('mobile');
        if(empty($mobile)){
            return $this->writeJson(400,'','手机号不能为空');
        }
        $password = $this->request()->getRequestParam('password');
        if(empty($password)){
            return $this->writeJson(400,'','密码不能为空');
        }
        $memberModel = new \App\Model\Member();
        $resultMember = $memberModel->findOne(['mobile'=>$mobile,'password'=>$password],'id,name');
        if($resultMember){
            $this->response()->setCookie('token',SysTools::generateToken($resultMember['id']),time()+SysConst::COOKIE_USER_SESSION_TTL);
            $this->response()->setCookie('user_id',$resultMember['id'],time()+SysConst::COOKIE_USER_SESSION_TTL);
            $this->response()->setCookie('name',urlencode($resultMember['name']),time()+SysConst::COOKIE_USER_SESSION_TTL);
            return $this->writeJson(200,'','验证通过');
        }else{
            return $this->writeJson(400,'','手机号或密码错误');
        }
    }
}



