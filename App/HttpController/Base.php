<?php
namespace App\HttpController;
use think\Template;
use EasySwoole\Http\AbstractInterface\Controller;
use EasySwoole\EasySwoole\Config;
class Base extends Controller {
    public $params;
    public function index() {

    }
    protected function onRequest(?string $action): ?bool
    {
        $this->getParams();
        return true;
    }
    public function getParams() {
        $params = $this->request()->getRequestParam();
        $params['page'] = !empty($params['page']) ? $params['page'] : 1;
        $params['size'] = !empty($params['size']) ? $params['size'] : 3;
        $params['from'] = ($params['page']-1) * $params['size'];
        $this->params = $params;
    }

    protected function fetch($tplName, $tplData = [])
    {
        $tplConfig = Config::getInstance()->getConf('template');
        $engine = new Template($tplConfig);
        // 由于ThinkPHP的模板引擎是直接echo输出到页面
        // 这里我们打开缓冲区，让模板引擎输出到缓冲区，再获取到模板编译后的字符串
        ob_start();
        $engine->fetch($tplName, $tplData);
        $content = ob_get_clean();
        $this->response()->write($content);
    }
    function cfgValue($name, $default = null)
    {
        $value = Config::getInstance()->getConf($name);
        return is_null($value) ? $default : $value;
    }
    function setpass($value) {
        return md5(md5($value.'dourou_'));
    }
    function writeDataJson($statusCode = 200, $data = null, $msg = null)
    {
        if (!$this->response()->isEndResponse()) {
            $data = Array(
                "code" => $statusCode,
                "data" => $data,
                "msg" => $msg
            );
            $this->response()->write(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
            $this->response()->withHeader('Content-type', 'application/json;charset=utf-8');
            $this->response()->withStatus($statusCode);
            return true;
        } else {
            return false;
        }
    }
    public function  __time_tranx($the_time)
    {
        $now_time = time();
        $dur = $now_time - $the_time;
        if ($dur <= 0) {
            $mas =  '刚刚';
        } else {
            if ($dur < 60) {
                $mas =  $dur . '秒前';
            } else {
                if ($dur < 3600) {
                    $mas =  floor($dur / 60) . '分钟前';
                } else {
                    if ($dur < 86400) {
                        $mas =  floor($dur / 3600) . '小时前';
                    } else {
                        if ($dur < 259200) { //3天内
                            $mas =  floor($dur / 86400) . '天前';
                        } else {
                            $mas =  date("Y-m-d H:i:s",$the_time);
                        }
                    }
                }
            }
        }
        return $mas;
    }
}