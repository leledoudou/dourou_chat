<?php
namespace App\WebSocket\Controller;
use EasySwoole\Socket\AbstractInterface\Controller;
class Base extends Controller {
    public $params;

    protected function onRequest(?string $actionName):bool
    {
        $this->getParams();
        return true;
    }
    public function getParams() {
        $params = $this->caller()->getArgs('data');
        $this->params = $params;
    }
}