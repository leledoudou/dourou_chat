<?php
namespace App\WebSocket\Controller;
use EasySwoole\Socket\AbstractInterface\Controller;
use EasySwoole\EasySwoole\ServerManager;
use EasySwoole\EasySwoole\Swoole\Task\TaskManager;
use App\WebSocket\Storage\Room;
use function foo\func;

class Test extends Controller {
    function hello()
    {
        $this->response()->setMessage('call hello with arg:'. json_encode($this->caller()->getArgs()));
    }
    function nima()
    {
        $this->response()->setMessage('call nima with arg:'. json_encode($this->caller()->getArgs()));
    }
    public function who(){
        $this->response()->setMessage('your fd is '. $this->caller()->getClient()->getFd());
    }
    public function qunliao() {
        $server = ServerManager::getInstance()->getSwooleServer();
        foreach($server->connections as $fd){
            var_dump($fd);
            $server->push($fd , json_encode($this->caller()->getArgs()));//循环广播
        }
    }
    function delay()
    {
        $this->response()->setMessage('this is delay action');
        $client = $this->caller()->getClient();

        // 异步推送, 这里直接 use fd也是可以的
        TaskManager::async(function () use ($client){
            $server = ServerManager::getInstance()->getSwooleServer();
            $i = 0;
            while ($i < 5) {
                sleep(1);
                $server->push($client->getFd(),'push in http at '. date('H:i:s'));
                $i++;
            }

        });
    }
    //进入房间
    public function intoRoom() {
//        var_dump($this->caller()->getArgs());
        $param =$this->caller()->getArgs('data');
        $userId = $param['userId'];
        $userName = $param['name'];
        $roomId = $param['roomId'];
        $fd = $this->caller()->getClient()->getFd();
        Room::login($userId,$fd);
        Room::joinRoom($roomId, $fd, $userId, $userName);

        //异步推送
        $server = ServerManager::getInstance()->getSwooleServer();
//        $message = json_encode([
//            'type'=>'system',
//            'action'=>'join_room',
//            'userId'=>$userId,
//            'userName'=>$userName,
//            'message'=>$userName.'进入房间'
//        ]);
//        $server->push($fd,$message);
//        foreach ($server->connections as $fd) {
//
//            // 需要先判断是否是正确的websocket连接，否则有可能会push失败
//            if ($server->isEstablished($fd)) {
//                $server->push($fd,$message);
//            }
//
//        }
        TaskManager::async(function ()use($userId,$userName,$roomId){
            $server = ServerManager::getInstance()->getSwooleServer();
            $message = json_encode([
                    'type'=>'system',
                    'action'=>'join_room',
                    'userId'=>$userId,
                    'userName'=>$userName,
                    'message'=>$userName.'进入房间'
            ]);
            $list = Room::selectRoomFd($roomId);
            foreach ($list as $fd) {
                if ($server->isEstablished($fd)) {
                    $server->push($fd,$message);
                }
            }
        });
    }
    /**
     * 发送信息到房间
     */
    public function sendToRoom()
    {
        $param =$this->caller()->getArgs('data');
        $message = $param['message'];
        $roomId = $param['roomId'];
        $fromUserId = $param['fromUserId'];
        //异步推送
        TaskManager::async(function ()use($fromUserId,$roomId, $message){
            $server = ServerManager::getInstance()->getSwooleServer();
            $list = Room::selectRoomFd($roomId);
            $resultMessage = json_encode([
                'type'=>'system',
                'action'=>'send_to_room',
                'message'=> Room::getUserName($fromUserId).'说:'.$message
            ]);
            foreach ($list as $fd) {
                if ($server->isEstablished($fd)) {
                    $server->push($fd, $resultMessage);
                }

            }
        });
    }

    /**
     * 发送私聊
     */
    public function sendToUser()
    {
        $param =$this->caller()->getArg('data');
        $message = $param['message'];
        $fromUserId = $param['fromUserId'];
        $userId = $param['userId'];
        //异步推送
        TaskManager::async(function ()use($fromUserId,$userId, $message){
            $server = ServerManager::getInstance()->getSwooleServer();
            $resultMessage = json_encode([
                'type'=>'system',
                'action'=>'send_to_user',
                'message'=>Room::getUserName($fromUserId).'说:'.$message
            ]);
            $fdList = Room::getUserFd($userId);
            foreach ($fdList as $fd) {
                $server->push($fd, $resultMessage);
            }
            $fdList = Room::getUserFd($fromUserId);
            foreach ($fdList as $fd) {
                $server->push($fd, $resultMessage);
            }
        });
    }
}