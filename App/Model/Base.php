<?php
namespace App\Model;
use think\Model;
class Base extends Model {

    public function getDataByCondition($condition = [],$field=[],$from=0, $size = 12,$order = '') {

        $result = $this->where($condition)
            ->field($field)
            ->limit($from, $size)
            ->order($order)
            ->select();
        return $result;
    }
    public function getDataCountByCondition($condition = []) {

        return $this->where($condition)
            ->count();
    }
}