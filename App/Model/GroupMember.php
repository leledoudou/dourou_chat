<?php
namespace App\Model;
use App\Model\Base;
class GroupMember extends Base {
    public $table = 'group_member';

    public function inserGroupMember($data) {
        $result = $this->insertGetId($data);
        return $result ?? null;
    }
}