<?php
namespace App\Model;
use App\Model\Base;
use think\Model;
class User extends Model {
//    public $table = 'user';
    public function getUserByName($username) {
        if (empty($username)) {
            return [];
        }
        $result  = $this->where('username','eq',$username)->find();
        return $result ?? [];
    }
    public function inserUser($data) {
        $result = $this->insertGetId($data);
        return $result;
    }
}