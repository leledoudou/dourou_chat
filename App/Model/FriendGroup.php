<?php
namespace App\Model;
use App\Model\Base;
class FriendGroup extends Base {
    public $table = 'friend_group';

    public function inserFriendGroup($data) {
        $result = $this->insertGetId($data);
        return $result ?? null;
    }
}