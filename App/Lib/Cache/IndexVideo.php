<?
namespace App\Lib\Cache;
use EasySwoole\EasySwoole\Config as GConfig;
use App\Lib\Redis\Redis;
class IndexVideo {
    public function getCache($catid) {
        switch (GConfig::getInstance()->getConf('INDEX_CACHE_TYPE')) {
            case 'file':
                //磁盘
                $videoFile = EASYSWOOLE_ROOT.'/data/'.$catid.'.json';
                $videoData = is_file($videoFile) ? json_decode(file_get_contents($videoFile)) : [] ;
                return $videoData;
                break;
            case 'redis':
                //内存
                $videoData = json_decode(Redis::getInstance()->get('index_video_'.$catid));
                return $videoData;
                break;
            default:
                //内存
                $videoData = json_decode(Redis::getInstance()->get('index_video_'.$catid));
                return $videoData;
                break;
        }
    }
    public  function setCache($catid,$data) {
        switch (GConfig::getInstance()->getConf('INDEX_CACHE_TYPE')) {
            case 'file':
                //存入本地磁盘中
                $flag = file_put_contents(EASYSWOOLE_ROOT.'/data/'.$catid.'.json',json_encode($data));
                return $flag;
                break;
            case 'redis':
                //redis  存入内存中，这样读取速度比存入磁盘更快
                $flag = Redis::getInstance()->set('index_video_'.$catid,json_encode($data));
                return $flag;
                break;
            default:
                //redis  存入内存中，这样读取速度比存入磁盘更快
                $flag = Redis::getInstance()->set('index_video_'.$catid,json_encode($data));
                return $flag;
                break;
        }
    }
}