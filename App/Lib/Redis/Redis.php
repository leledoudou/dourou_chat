<?
namespace App\Lib\Redis;
use EasySwoole\Component\Singleton;
class Redis {
    use Singleton;
    public $redis = '';
    private function __construct()
    {
        if (!extension_loaded('redis')) {
            throw new \Exception('redis.so文件不存在');
        }
        try {
            $this->redis = new \Redis();
            $result = $this->redis->connect('127.0.0.1',6379,3);
        } catch (\Exception $e) {
            throw new \Exception('redis服务异常');
        }
        if ($result == false) {
            throw new \Exception('redis连接失败');
        }
    }
    public function get($key) {
        if (empty($key)) {
            return '';
        }
        return $this->redis->get($key);
    }
    public function set($key,$value,$time='') {
        if (empty($key)) {
            return '';
        }
        return $this->redis->set($key,$value,$time);
    }
    public function lPop($key) {
        if (empty($key)) {
            return '';
        }
        return $this->redis->lPop($key);
    }
    public function rPush($key,$value) {
        if (empty($key)) {
            return '';
        }
        return $this->redis->rPush($key,$value);
    }
    public function zincrby($key,$number,$member) {
        if (empty($key) || empty($member)) {
            return false;
        }
        return $this->redis->zIncrBy($key,$number,$member);
    }
    public function zrevrange($key,$start,$top,$type) {
        if (empty($key)) {
            return false;
        }
        return $this->redis->zRevRange($key,$start,$top,$type);
    }
    public function __call($name, $arguments)
    {
        return $this->redis->$name(...$arguments);
    }

}