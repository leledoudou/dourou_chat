<?php
namespace App\Lib\Upload;
use App\Lib\Upload\Base;
class Video extends Base {
    public $fileType = 'file';
    public $maxSize = 122*1024*1024*1024;
    /**
     * @var array 文件后缀
     */
    public $fileExtTypes = [
        'mp4',
        'x-flv',
        'mp3',
        'txt',
        'doc',
        'docx',
        'document',
        'pdf',
        'msword'
    ];
}