<?php
namespace App\Lib\Upload;
use App\Lib\Upload\Base;
class Image extends Base {
    public $fileType = 'file';
    public $maxSize = 10*1024*1024;
    /**
     * @var array 文件后缀
     */
    public $fileExtTypes = [
        'png',
        'jpeg',
        'jpg',
        'gif'
    ];
}