<?php
/**
 * Created by PhpStorm.
 * User: yf
 * Date: 2019-01-01
 * Time: 20:06
 */

return [
    'SERVER_NAME' => "EasySwoole",
    'MAIN_SERVER' => [
        'LISTEN_ADDRESS' => '0.0.0.0',
        'PORT' => 9501,
        'SERVER_TYPE' => EASYSWOOLE_WEB_SOCKET_SERVER, //可选为 EASYSWOOLE_SERVER  EASYSWOOLE_WEB_SERVER EASYSWOOLE_WEB_SOCKET_SERVER,EASYSWOOLE_REDIS_SERVER
        'SOCK_TYPE' => SWOOLE_TCP,
        'RUN_MODEL' => SWOOLE_PROCESS,
        'SETTING' => [
            'worker_num' => 8,
            'task_worker_num' => 8,
            'reload_async' => true,
            'task_enable_coroutine' => true,
            'document_root'=> EASYSWOOLE_ROOT . '/',
            'enable_static_handler' => true,
            'max_wait_time'=>3
        ],
    ],
    'template' => [
        // 模板文件目录
        'view_path'   => './views/',
        // 编译后的模板文件缓存目录
        'cache_path'  => './runtime/',
        // 模板文件后缀
        'view_suffix' => 'html',
    ],
    'TEMP_DIR' => null,
    'LOG_DIR' => null,
    'database' => [
        // 数据库类型
        'type'            => 'mysql',
        // 服务器地址
        'hostname'        => 'localhost',
        // 数据库名
        'database'        => 'my_haozhuanapp_c',
        // 用户名
        'username'        => 'my_haozhuanapp_c',
        // 密码
        'password'        => 'A7nETDhzBNjjW4LJ',
        // 端口
        'hostport'        => '3306',
        // 数据库表前缀
        'prefix'          => '',
        // 是否需要断线重连
        'break_reconnect' => true,
    ],
    'HOST'           => 'http://easy.haozhuanapp.com:9501',
    'WEBSOCKET_HOST' => 'ws://easy.haozhuanapp.com:9501'
];
