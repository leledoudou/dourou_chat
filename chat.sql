-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2019-11-21 10:59:22
-- 服务器版本： 5.6.44-log
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chat`
--

-- --------------------------------------------------------

--
-- 表的结构 `chat_record`
--

CREATE TABLE `chat_record` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL DEFAULT '0' COMMENT '是群聊消息记录的话 此id为0',
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '如果不为0说明是群聊',
  `content` varchar(1000) NOT NULL DEFAULT '',
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='聊天记录' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `chat_record`
--

INSERT INTO `chat_record` (`id`, `user_id`, `friend_id`, `group_id`, `content`, `time`) VALUES
(80, 10014, 10013, 0, '11', 1560922127),
(81, 10013, 10014, 0, ' q', 1560922140),
(82, 10014, 10013, 0, 's', 1560922159),
(83, 10014, 10013, 0, 'assasasa', 1560922177),
(84, 10013, 10014, 0, 'face[晕] ', 1560922183),
(85, 10014, 10013, 0, '2w2', 1560922227),
(86, 10013, 10014, 0, '32', 1560922244),
(87, 10014, 10013, 0, 'www', 1560922260),
(88, 10013, 10014, 0, '12312', 1560922518),
(89, 10013, 10014, 0, '213', 1560922524),
(90, 10013, 0, 10014, '1', 1560923445),
(91, 10020, 0, 10015, '换个', 1574217156),
(92, 10013, 0, 10015, '5454', 1574217243),
(93, 10013, 0, 10015, 'fgh ', 1574217459),
(94, 10013, 0, 10015, '34', 1574217534),
(95, 10020, 0, 10015, '哈哈', 1574217600),
(96, 10020, 0, 10015, '5555', 1574217644),
(97, 10013, 0, 10015, '534', 1574217650),
(98, 10013, 0, 10015, 'h', 1574217700),
(99, 10013, 0, 10015, '555', 1574217793),
(100, 10020, 0, 10015, '哈哈哈哈', 1574217935),
(101, 10013, 0, 10015, 'b ', 1574218721),
(102, 10020, 0, 10015, 'hhh', 1574218735),
(103, 10020, 0, 10015, 'hghg hgfhgh ', 1574218746),
(104, 10020, 0, 10015, 'nimaniam', 1574218750),
(105, 10013, 0, 10015, 'cdascdc ', 1574218754),
(106, 10013, 0, 10015, 'gfhghgh hgh ggh ghghg', 1574218845),
(107, 10013, 0, 10015, '000', 1574218900),
(108, 10020, 0, 10015, 'ppp', 1574218969),
(109, 10020, 0, 10015, 'lll', 1574218973),
(110, 10020, 10014, 0, '6', 1574231610),
(111, 10020, 10014, 0, '777', 1574231653),
(112, 10014, 10020, 0, '888', 1574231667),
(113, 10014, 10020, 0, '哦哦哦', 1574231673),
(114, 10014, 10020, 0, 'img[http://easy.haozhuanapp.com:9501/webroot/file/2019/11/4741f01fc4539bf5.jpg]', 1574231706),
(115, 10020, 0, 10014, '就', 1574236036),
(116, 10028, 0, 10014, 'v刹v', 1574238591),
(117, 10028, 0, 10014, '45454', 1574238594),
(118, 10020, 10014, 0, '32', 1574238649),
(119, 10020, 10014, 0, '反对反对', 1574238652),
(120, 10020, 10014, 0, '6765767', 1574238704),
(121, 10020, 10014, 0, '复古风格', 1574238745),
(122, 10020, 10014, 0, '复古风格', 1574238756),
(123, 10020, 10014, 0, '32', 1574238817),
(124, 10020, 0, 10019, '6', 1574240509),
(125, 10020, 10014, 0, '67887', 1574242894),
(126, 10020, 10014, 0, '878787', 1574242896),
(127, 10020, 10014, 0, '贵航股份和规范化g', 1574242898),
(128, 10020, 10014, 0, '回个话广告g', 1574242899),
(129, 10014, 10020, 0, '8987989', 1574242949),
(130, 10020, 10014, 0, '09809809', 1574242982),
(131, 10020, 10014, 0, '00000000000000000000000', 1574243057),
(132, 10014, 0, 10015, '878787', 1574243135),
(133, 10020, 10014, 0, 'img[http://easy.haozhuanapp.com:9501/webroot/file/2019/11/b80dbf7be392de91.png]', 1574243601),
(134, 10020, 10014, 0, '777', 1574295866),
(135, 10014, 0, 10015, '56565', 1574298283),
(136, 10020, 10013, 0, '7777', 1574298824),
(137, 10020, 10013, 0, '999', 1574298902),
(138, 10020, 10014, 0, 'lkjljk', 1574298913),
(139, 10014, 10020, 0, '0000', 1574298933),
(140, 10014, 10020, 0, 'img[http://easy.haozhuanapp.com:9501/webroot/file/2019/11/3636a7f104bbbcc2.png]', 1574298950),
(141, 10014, 10020, 0, 'face[可怜] ', 1574298964);

-- --------------------------------------------------------

--
-- 表的结构 `friend`
--

CREATE TABLE `friend` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  `friend_group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='好友表' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `friend`
--

INSERT INTO `friend` (`id`, `user_id`, `friend_id`, `friend_group_id`) VALUES
(84, 10013, 10014, 10),
(85, 10014, 10013, 11),
(86, 10014, 10020, 11),
(87, 10020, 10014, 17),
(88, 10013, 10020, 10),
(89, 10020, 10013, 17),
(90, 10025, 10020, 22),
(91, 10020, 10025, 17);

-- --------------------------------------------------------

--
-- 表的结构 `friend_group`
--

CREATE TABLE `friend_group` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `groupname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `friend_group`
--

INSERT INTO `friend_group` (`id`, `user_id`, `groupname`) VALUES
(10, 10013, '默认分组'),
(11, 10014, '默认分组'),
(12, 10015, '默认分组'),
(13, 10016, '默认分组'),
(14, 10017, '默认分组'),
(15, 10018, '默认分组'),
(16, 10019, '默认分组'),
(17, 10020, '默认分组'),
(18, 10021, '默认分组'),
(19, 10022, '默认分组'),
(20, 10023, '默认分组'),
(21, 10024, '默认分组'),
(22, 10025, '默认分组'),
(23, 10026, '默认分组'),
(24, 10027, '默认分组'),
(25, 10028, '默认分组');

-- --------------------------------------------------------

--
-- 表的结构 `group`
--

CREATE TABLE `group` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT '群组所属用户id,群主',
  `groupname` varchar(255) NOT NULL COMMENT '群名',
  `avatar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='群组' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `group`
--

INSERT INTO `group` (`id`, `user_id`, `groupname`, `avatar`) VALUES
(10014, 10013, '测试', '/static/upload/5d09c956c0ccctimg.jpg'),
(10015, 10020, '吹水群', '/static/upload/5d09cdca1dffb3-1G123203S6-50.jpg'),
(10016, 10020, '大哈巴群', ''),
(10017, 10020, '娜娜群', ''),
(10018, 10020, '传递出对', ''),
(10019, 10020, '传递出对', '/webroot/file/2019/11/afc4917557426df8.png');

-- --------------------------------------------------------

--
-- 表的结构 `group_member`
--

CREATE TABLE `group_member` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `group_member`
--

INSERT INTO `group_member` (`id`, `group_id`, `user_id`) VALUES
(39, 10001, 10013),
(40, 10001, 10014),
(41, 10014, 10013),
(42, 10014, 10014),
(43, 10015, 10014),
(44, 10015, 10013),
(45, 10001, 10015),
(46, 10001, 10016),
(47, 10001, 10017),
(48, 10001, 10018),
(49, 10001, 10019),
(50, 10001, 10020),
(51, 10015, 10020),
(52, 10014, 10020),
(53, 10001, 10021),
(54, 10014, 10021),
(55, 10015, 10021),
(56, 10001, 10022),
(57, 10014, 10022),
(58, 10015, 10022),
(59, 10001, 10023),
(60, 10014, 10023),
(61, 10015, 10023),
(62, 10001, 10024),
(63, 10014, 10024),
(64, 10015, 10024),
(65, 10001, 10025),
(66, 10014, 10025),
(67, 10015, 10025),
(68, 10001, 10026),
(69, 10015, 10026),
(70, 10001, 10027),
(71, 10014, 10027),
(72, 10015, 10027),
(73, 10001, 10028),
(74, 10014, 10028),
(75, 10016, 10020),
(76, 10017, 10020),
(77, 10018, 10020),
(78, 10019, 10020),
(79, 10019, 10014),
(80, 10016, 10014);

-- --------------------------------------------------------

--
-- 表的结构 `offline_message`
--

CREATE TABLE `offline_message` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `data` varchar(1000) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未发送 1已发送'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='离线消息表' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `offline_message`
--

INSERT INTO `offline_message` (`id`, `user_id`, `data`, `status`) VALUES
(19, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10015,\"type\":\"group\",\"content\":\"\\u6362\\u4e2a\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574217156000}', 1),
(20, 10013, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10015,\"type\":\"group\",\"content\":\"\\u6362\\u4e2a\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574217156000}', 1),
(21, 10014, '{\"username\":\"jin(10013)\",\"avatar\":\"\\/static\\/upload\\/5d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"5454\",\"cid\":0,\"mine\":false,\"fromid\":10013,\"timestamp\":1574217243000}', 1),
(22, 10020, '{\"username\":\"jin(10013)\",\"avatar\":\"\\/static\\/upload\\/5d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"5454\",\"cid\":0,\"mine\":false,\"fromid\":10013,\"timestamp\":1574217243000}', 1),
(23, 10014, '{\"username\":\"jin(10013)\",\"avatar\":\"\\/static\\/upload\\/5d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"fgh \",\"cid\":0,\"mine\":false,\"fromid\":10013,\"timestamp\":1574217459000}', 1),
(24, 10020, '{\"username\":\"jin(10013)\",\"avatar\":\"\\/static\\/upload\\/5d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"fgh \",\"cid\":0,\"mine\":false,\"fromid\":10013,\"timestamp\":1574217459000}', 1),
(25, 10014, '{\"username\":\"jin(10013)\",\"avatar\":\"\\/static\\/upload\\/5d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"34\",\"cid\":0,\"mine\":false,\"fromid\":10013,\"timestamp\":1574217534000}', 1),
(26, 10020, '{\"username\":\"jin(10013)\",\"avatar\":\"\\/static\\/upload\\/5d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"34\",\"cid\":0,\"mine\":false,\"fromid\":10013,\"timestamp\":1574217534000}', 1),
(27, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10015,\"type\":\"group\",\"content\":\"\\u54c8\\u54c8\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574217599000}', 1),
(28, 10013, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10015,\"type\":\"group\",\"content\":\"\\u54c8\\u54c8\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574217599000}', 1),
(29, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10015,\"type\":\"group\",\"content\":\"5555\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574217644000}', 1),
(30, 10013, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10015,\"type\":\"group\",\"content\":\"5555\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574217644000}', 1),
(31, 10014, '{\"username\":\"jin(10013)\",\"avatar\":\"\\/static\\/upload\\/5d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"534\",\"cid\":0,\"mine\":false,\"fromid\":10013,\"timestamp\":1574217650000}', 1),
(32, 10020, '{\"username\":\"jin(10013)\",\"avatar\":\"\\/static\\/upload\\/5d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"534\",\"cid\":0,\"mine\":false,\"fromid\":10013,\"timestamp\":1574217650000}', 1),
(33, 10014, '{\"username\":\"jin(10013)\",\"avatar\":\"\\/static\\/upload\\/5d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"h\",\"cid\":0,\"mine\":false,\"fromid\":10013,\"timestamp\":1574217700000}', 1),
(34, 10020, '{\"username\":\"jin(10013)\",\"avatar\":\"\\/static\\/upload\\/5d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"h\",\"cid\":0,\"mine\":false,\"fromid\":10013,\"timestamp\":1574217700000}', 1),
(35, 10014, '{\"username\":\"jin(10013)\",\"avatar\":\"\\/static\\/upload\\/5d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"555\",\"cid\":0,\"mine\":false,\"fromid\":10013,\"timestamp\":1574217793000}', 1),
(36, 10020, '{\"username\":\"jin(10013)\",\"avatar\":\"\\/static\\/upload\\/5d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"555\",\"cid\":0,\"mine\":false,\"fromid\":10013,\"timestamp\":1574217793000}', 1),
(37, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10015,\"type\":\"group\",\"content\":\"\\u54c8\\u54c8\\u54c8\\u54c8\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574217935000}', 1),
(38, 10013, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10015,\"type\":\"group\",\"content\":\"\\u54c8\\u54c8\\u54c8\\u54c8\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574217935000}', 1),
(39, 10014, '{\"username\":\"jin(10013)\",\"avatar\":\"\\/static\\/upload\\/5d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"b \",\"cid\":0,\"mine\":false,\"fromid\":10013,\"timestamp\":1574218721000}', 1),
(40, 10020, '{\"username\":\"jin(10013)\",\"avatar\":\"\\/static\\/upload\\/5d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"b \",\"cid\":0,\"mine\":false,\"fromid\":10013,\"timestamp\":1574218721000}', 1),
(41, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10015,\"type\":\"group\",\"content\":\"hhh\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574218735000}', 1),
(42, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10015,\"type\":\"group\",\"content\":\"hghg hgfhgh \",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574218746000}', 1),
(43, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10015,\"type\":\"group\",\"content\":\"nimaniam\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574218750000}', 1),
(44, 10014, '{\"username\":\"jin(10013)\",\"avatar\":\"\\/static\\/upload\\/5d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"cdascdc \",\"cid\":0,\"mine\":false,\"fromid\":10013,\"timestamp\":1574218754000}', 1),
(45, 10014, '{\"username\":\"jin(10013)\",\"avatar\":\"\\/static\\/upload\\/5d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"gfhghgh hgh ggh ghghg\",\"cid\":0,\"mine\":false,\"fromid\":10013,\"timestamp\":1574218845000}', 1),
(46, 10014, '{\"username\":\"jin(10013)\",\"avatar\":\"\\/static\\/upload\\/5d09c6a5b6def15337177846531748ac16fb.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"000\",\"cid\":0,\"mine\":false,\"fromid\":10013,\"timestamp\":1574218900000}', 1),
(47, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10015,\"type\":\"group\",\"content\":\"ppp\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574218968000}', 1),
(48, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10015,\"type\":\"group\",\"content\":\"lll\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574218973000}', 1),
(49, 10014, '{\"type\":\"msgBox\",\"count\":1}', 1),
(50, 10014, '{\"type\":\"msgBox\",\"count\":2}', 1),
(51, 10013, '{\"type\":\"msgBox\",\"count\":1}', 1),
(52, 10013, '{\"type\":\"msgBox\",\"count\":2}', 1),
(53, 10013, '{\"type\":\"msgBox\",\"count\":3}', 1),
(54, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10020,\"type\":\"friend\",\"content\":\"6\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574231610000}', 1),
(55, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10020,\"type\":\"friend\",\"content\":\"777\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574231653000}', 1),
(56, 10013, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10014,\"type\":\"group\",\"content\":\"\\u5c31\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574236036000}', 0),
(57, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10014,\"type\":\"group\",\"content\":\"\\u5c31\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574236036000}', 1),
(58, 10013, '{\"username\":\"fbfvbvb(10028)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/442c9a93e110a715.jpg\",\"id\":10014,\"type\":\"group\",\"content\":\"v\\u5239v\",\"cid\":0,\"mine\":false,\"fromid\":10028,\"timestamp\":1574238591000}', 0),
(59, 10014, '{\"username\":\"fbfvbvb(10028)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/442c9a93e110a715.jpg\",\"id\":10014,\"type\":\"group\",\"content\":\"v\\u5239v\",\"cid\":0,\"mine\":false,\"fromid\":10028,\"timestamp\":1574238591000}', 1),
(60, 10020, '{\"username\":\"fbfvbvb(10028)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/442c9a93e110a715.jpg\",\"id\":10014,\"type\":\"group\",\"content\":\"v\\u5239v\",\"cid\":0,\"mine\":false,\"fromid\":10028,\"timestamp\":1574238591000}', 1),
(61, 10021, '{\"username\":\"fbfvbvb(10028)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/442c9a93e110a715.jpg\",\"id\":10014,\"type\":\"group\",\"content\":\"v\\u5239v\",\"cid\":0,\"mine\":false,\"fromid\":10028,\"timestamp\":1574238591000}', 1),
(62, 10022, '{\"username\":\"fbfvbvb(10028)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/442c9a93e110a715.jpg\",\"id\":10014,\"type\":\"group\",\"content\":\"v\\u5239v\",\"cid\":0,\"mine\":false,\"fromid\":10028,\"timestamp\":1574238591000}', 0),
(63, 10023, '{\"username\":\"fbfvbvb(10028)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/442c9a93e110a715.jpg\",\"id\":10014,\"type\":\"group\",\"content\":\"v\\u5239v\",\"cid\":0,\"mine\":false,\"fromid\":10028,\"timestamp\":1574238591000}', 0),
(64, 10025, '{\"username\":\"fbfvbvb(10028)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/442c9a93e110a715.jpg\",\"id\":10014,\"type\":\"group\",\"content\":\"v\\u5239v\",\"cid\":0,\"mine\":false,\"fromid\":10028,\"timestamp\":1574238591000}', 1),
(65, 10027, '{\"username\":\"fbfvbvb(10028)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/442c9a93e110a715.jpg\",\"id\":10014,\"type\":\"group\",\"content\":\"v\\u5239v\",\"cid\":0,\"mine\":false,\"fromid\":10028,\"timestamp\":1574238591000}', 0),
(66, 10013, '{\"username\":\"fbfvbvb(10028)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/442c9a93e110a715.jpg\",\"id\":10014,\"type\":\"group\",\"content\":\"45454\",\"cid\":0,\"mine\":false,\"fromid\":10028,\"timestamp\":1574238594000}', 0),
(67, 10014, '{\"username\":\"fbfvbvb(10028)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/442c9a93e110a715.jpg\",\"id\":10014,\"type\":\"group\",\"content\":\"45454\",\"cid\":0,\"mine\":false,\"fromid\":10028,\"timestamp\":1574238594000}', 1),
(68, 10020, '{\"username\":\"fbfvbvb(10028)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/442c9a93e110a715.jpg\",\"id\":10014,\"type\":\"group\",\"content\":\"45454\",\"cid\":0,\"mine\":false,\"fromid\":10028,\"timestamp\":1574238594000}', 1),
(69, 10021, '{\"username\":\"fbfvbvb(10028)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/442c9a93e110a715.jpg\",\"id\":10014,\"type\":\"group\",\"content\":\"45454\",\"cid\":0,\"mine\":false,\"fromid\":10028,\"timestamp\":1574238594000}', 1),
(70, 10022, '{\"username\":\"fbfvbvb(10028)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/442c9a93e110a715.jpg\",\"id\":10014,\"type\":\"group\",\"content\":\"45454\",\"cid\":0,\"mine\":false,\"fromid\":10028,\"timestamp\":1574238594000}', 0),
(71, 10023, '{\"username\":\"fbfvbvb(10028)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/442c9a93e110a715.jpg\",\"id\":10014,\"type\":\"group\",\"content\":\"45454\",\"cid\":0,\"mine\":false,\"fromid\":10028,\"timestamp\":1574238594000}', 0),
(72, 10025, '{\"username\":\"fbfvbvb(10028)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/442c9a93e110a715.jpg\",\"id\":10014,\"type\":\"group\",\"content\":\"45454\",\"cid\":0,\"mine\":false,\"fromid\":10028,\"timestamp\":1574238594000}', 1),
(73, 10027, '{\"username\":\"fbfvbvb(10028)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/442c9a93e110a715.jpg\",\"id\":10014,\"type\":\"group\",\"content\":\"45454\",\"cid\":0,\"mine\":false,\"fromid\":10028,\"timestamp\":1574238594000}', 0),
(74, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10020,\"type\":\"friend\",\"content\":\"32\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574238649000}', 1),
(75, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10020,\"type\":\"friend\",\"content\":\"\\u53cd\\u5bf9\\u53cd\\u5bf9\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574238652000}', 1),
(76, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10020,\"type\":\"friend\",\"content\":\"6765767\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574238704000}', 1),
(77, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10020,\"type\":\"friend\",\"content\":\"67887\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574242894000}', 1),
(78, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10020,\"type\":\"friend\",\"content\":\"878787\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574242896000}', 1),
(79, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10020,\"type\":\"friend\",\"content\":\"\\u8d35\\u822a\\u80a1\\u4efd\\u548c\\u89c4\\u8303\\u5316g\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574242898000}', 1),
(80, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10020,\"type\":\"friend\",\"content\":\"\\u56de\\u4e2a\\u8bdd\\u5e7f\\u544ag\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574242899000}', 1),
(81, 10013, '{\"username\":\"\\u524d\\u7aef\\u5de5\\u7a0b\\u5e08(10014)\",\"avatar\":\"\\/static\\/upload\\/5d09c7da7bc97tx20218.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"878787\",\"cid\":0,\"mine\":false,\"fromid\":10014,\"timestamp\":1574243135000}', 0),
(82, 10021, '{\"username\":\"\\u524d\\u7aef\\u5de5\\u7a0b\\u5e08(10014)\",\"avatar\":\"\\/static\\/upload\\/5d09c7da7bc97tx20218.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"878787\",\"cid\":0,\"mine\":false,\"fromid\":10014,\"timestamp\":1574243135000}', 1),
(83, 10022, '{\"username\":\"\\u524d\\u7aef\\u5de5\\u7a0b\\u5e08(10014)\",\"avatar\":\"\\/static\\/upload\\/5d09c7da7bc97tx20218.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"878787\",\"cid\":0,\"mine\":false,\"fromid\":10014,\"timestamp\":1574243135000}', 0),
(84, 10023, '{\"username\":\"\\u524d\\u7aef\\u5de5\\u7a0b\\u5e08(10014)\",\"avatar\":\"\\/static\\/upload\\/5d09c7da7bc97tx20218.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"878787\",\"cid\":0,\"mine\":false,\"fromid\":10014,\"timestamp\":1574243135000}', 0),
(85, 10024, '{\"username\":\"\\u524d\\u7aef\\u5de5\\u7a0b\\u5e08(10014)\",\"avatar\":\"\\/static\\/upload\\/5d09c7da7bc97tx20218.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"878787\",\"cid\":0,\"mine\":false,\"fromid\":10014,\"timestamp\":1574243135000}', 0),
(86, 10025, '{\"username\":\"\\u524d\\u7aef\\u5de5\\u7a0b\\u5e08(10014)\",\"avatar\":\"\\/static\\/upload\\/5d09c7da7bc97tx20218.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"878787\",\"cid\":0,\"mine\":false,\"fromid\":10014,\"timestamp\":1574243135000}', 1),
(87, 10026, '{\"username\":\"\\u524d\\u7aef\\u5de5\\u7a0b\\u5e08(10014)\",\"avatar\":\"\\/static\\/upload\\/5d09c7da7bc97tx20218.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"878787\",\"cid\":0,\"mine\":false,\"fromid\":10014,\"timestamp\":1574243135000}', 0),
(88, 10027, '{\"username\":\"\\u524d\\u7aef\\u5de5\\u7a0b\\u5e08(10014)\",\"avatar\":\"\\/static\\/upload\\/5d09c7da7bc97tx20218.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"878787\",\"cid\":0,\"mine\":false,\"fromid\":10014,\"timestamp\":1574243135000}', 0),
(89, 10014, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10020,\"type\":\"friend\",\"content\":\"img[http:\\/\\/easy.haozhuanapp.com:9501\\/webroot\\/file\\/2019\\/11\\/b80dbf7be392de91.png]\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574243601000}', 1),
(90, 10013, '{\"username\":\"\\u524d\\u7aef\\u5de5\\u7a0b\\u5e08(10014)\",\"avatar\":\"\\/static\\/upload\\/5d09c7da7bc97tx20218.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"56565\",\"cid\":0,\"mine\":false,\"fromid\":10014,\"timestamp\":1574298283000}', 0),
(91, 10021, '{\"username\":\"\\u524d\\u7aef\\u5de5\\u7a0b\\u5e08(10014)\",\"avatar\":\"\\/static\\/upload\\/5d09c7da7bc97tx20218.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"56565\",\"cid\":0,\"mine\":false,\"fromid\":10014,\"timestamp\":1574298283000}', 0),
(92, 10022, '{\"username\":\"\\u524d\\u7aef\\u5de5\\u7a0b\\u5e08(10014)\",\"avatar\":\"\\/static\\/upload\\/5d09c7da7bc97tx20218.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"56565\",\"cid\":0,\"mine\":false,\"fromid\":10014,\"timestamp\":1574298283000}', 0),
(93, 10023, '{\"username\":\"\\u524d\\u7aef\\u5de5\\u7a0b\\u5e08(10014)\",\"avatar\":\"\\/static\\/upload\\/5d09c7da7bc97tx20218.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"56565\",\"cid\":0,\"mine\":false,\"fromid\":10014,\"timestamp\":1574298283000}', 0),
(94, 10024, '{\"username\":\"\\u524d\\u7aef\\u5de5\\u7a0b\\u5e08(10014)\",\"avatar\":\"\\/static\\/upload\\/5d09c7da7bc97tx20218.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"56565\",\"cid\":0,\"mine\":false,\"fromid\":10014,\"timestamp\":1574298283000}', 0),
(95, 10025, '{\"username\":\"\\u524d\\u7aef\\u5de5\\u7a0b\\u5e08(10014)\",\"avatar\":\"\\/static\\/upload\\/5d09c7da7bc97tx20218.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"56565\",\"cid\":0,\"mine\":false,\"fromid\":10014,\"timestamp\":1574298283000}', 1),
(96, 10026, '{\"username\":\"\\u524d\\u7aef\\u5de5\\u7a0b\\u5e08(10014)\",\"avatar\":\"\\/static\\/upload\\/5d09c7da7bc97tx20218.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"56565\",\"cid\":0,\"mine\":false,\"fromid\":10014,\"timestamp\":1574298283000}', 0),
(97, 10027, '{\"username\":\"\\u524d\\u7aef\\u5de5\\u7a0b\\u5e08(10014)\",\"avatar\":\"\\/static\\/upload\\/5d09c7da7bc97tx20218.jpg\",\"id\":10015,\"type\":\"group\",\"content\":\"56565\",\"cid\":0,\"mine\":false,\"fromid\":10014,\"timestamp\":1574298283000}', 0),
(98, 10025, '{\"type\":\"msgBox\",\"count\":1}', 1),
(99, 10025, '{\"type\":\"msgBox\",\"count\":2}', 1),
(100, 10026, '{\"type\":\"msgBox\",\"count\":1}', 0),
(101, 10013, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10020,\"type\":\"friend\",\"content\":\"7777\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574298824000}', 0),
(102, 10013, '{\"username\":\"\\u591a\\u8089(10020)\",\"avatar\":\"\\/webroot\\/file\\/2019\\/11\\/8b7b2112e08ba8ca.png\",\"id\":10020,\"type\":\"friend\",\"content\":\"999\",\"cid\":0,\"mine\":false,\"fromid\":10020,\"timestamp\":1574298902000}', 0);

-- --------------------------------------------------------

--
-- 表的结构 `system_message`
--

CREATE TABLE `system_message` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT '接收用户id',
  `from_id` int(11) NOT NULL COMMENT '来源相关用户id',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '添加好友附言',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0好友请求 1请求结果通知',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未处理 1同意 2拒绝',
  `read` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未读 1已读，用来显示消息盒子数量',
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统消息表' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `system_message`
--

INSERT INTO `system_message` (`id`, `user_id`, `from_id`, `group_id`, `remark`, `type`, `status`, `read`, `time`) VALUES
(66, 10001, 10005, 4, '', 0, 2, 1, 1560917188),
(67, 10005, 10001, 0, '', 1, 2, 1, 1560917196),
(68, 10001, 10005, 4, '', 0, 1, 1, 1560917221),
(69, 10005, 10001, 0, '', 1, 1, 1, 1560917226),
(70, 10005, 10001, 3, '', 0, 1, 1, 1560917363),
(71, 10001, 10005, 0, '', 1, 1, 1, 1560917371),
(72, 10005, 10001, 3, '', 0, 1, 1, 1560917512),
(73, 10001, 10005, 0, '', 1, 1, 1, 1560917557),
(74, 10005, 10001, 3, '', 0, 1, 1, 1560917998),
(75, 10001, 10005, 0, '', 1, 1, 1, 1560918012),
(76, 10005, 10001, 3, '', 0, 2, 1, 1560918338),
(77, 10001, 10005, 0, '', 1, 2, 1, 1560918348),
(78, 10005, 10001, 3, '', 0, 2, 1, 1560918411),
(79, 10001, 10005, 0, '', 1, 2, 1, 1560918430),
(80, 10005, 10001, 3, '', 0, 2, 1, 1560918514),
(81, 10001, 10005, 0, '', 1, 2, 1, 1560918518),
(82, 10005, 10001, 3, '', 0, 2, 1, 1560918600),
(83, 10001, 10005, 0, '', 1, 2, 1, 1560918612),
(84, 10001, 10005, 4, '', 0, 1, 1, 1560920590),
(85, 10005, 10001, 0, '', 1, 1, 1, 1560920620),
(86, 10013, 10014, 11, '', 0, 1, 1, 1560922101),
(87, 10014, 10013, 0, '', 1, 1, 1, 1560922107),
(88, 10014, 10020, 17, '', 0, 2, 1, 1574221329),
(89, 10014, 10020, 17, 'fgfd g', 0, 2, 1, 1574221338),
(90, 10014, 10020, 17, 'fdgf ', 0, 1, 1, 1574221384),
(91, 10020, 10014, 0, '', 1, 1, 1, 1574228895),
(92, 10020, 10014, 0, '', 1, 2, 1, 1574229850),
(93, 10020, 10014, 0, '', 1, 2, 1, 1574229852),
(94, 10020, 10014, 0, '', 1, 2, 1, 1574229887),
(95, 10013, 10020, 17, '', 0, 0, 1, 1574230451),
(96, 10013, 10020, 17, '', 0, 1, 1, 1574230565),
(97, 10013, 10020, 17, '', 0, 2, 1, 1574230574),
(98, 10020, 10013, 0, '', 1, 2, 1, 1574230654),
(99, 10020, 10013, 0, '', 1, 1, 1, 1574230659),
(100, 10025, 10020, 17, '', 0, 0, 1, 1574298429),
(101, 10025, 10020, 17, '', 0, 1, 1, 1574298452),
(102, 10020, 10025, 0, '', 1, 1, 1, 1574298485),
(103, 10026, 10020, 17, '', 0, 0, 0, 1574298735);

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `avatar` varchar(255) NOT NULL COMMENT '头像',
  `nickname` varchar(255) NOT NULL COMMENT '昵称',
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL,
  `sign` varchar(255) NOT NULL COMMENT '签名',
  `status` varchar(255) NOT NULL DEFAULT 'online' COMMENT 'online在线 hide隐身 offline离线'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户表' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`id`, `avatar`, `nickname`, `username`, `password`, `sign`, `status`) VALUES
(10013, '/static/upload/5d09c6a5b6def15337177846531748ac16fb.jpg', 'jin', 'jin', 'd9ae6f2629a48726ec9f6c54ae6c19f7', '我是jin', 'offline'),
(10014, '/static/upload/5d09c7da7bc97tx20218.jpg', '前端工程师', 'test1', 'd9ae6f2629a48726ec9f6c54ae6c19f7', 'fdfd', 'online'),
(10015, '/static/upload/5d09ceac4de6312262La2-0.jpg', 'php工程师', 'test2', '$2y$10$ueMA2hy8x.Tan3nxZlpCmugUcViGCaV/cAeA4V5YX.yU.1kCtAtzq', '123', 'offline'),
(10016, '', 'gd', 'gdg', '$2y$10$vfKS8yY7mHYx6RVDdsDFaelln.058To.JGv/K2zOE/XKrL161mxCu', 'gd', 'online'),
(10019, '/webroot/file/2019/11/0a10c5d0867c50d9.png', '反对反对', '苟富贵', '$2y$10$ScMfql6gG4VqeMA4viQFi.hLEORBqyVVcZMsJjWU6tXlMjY/VNJmK', '发的', 'online'),
(10020, '/webroot/file/2019/11/8b7b2112e08ba8ca.png', '多肉', '多肉', 'd9ae6f2629a48726ec9f6c54ae6c19f7', 'heheh', 'online'),
(10021, '/webroot/file/2019/11/0772cbae02bc7b11.jpg', '问天', '111', 'd9ae6f2629a48726ec9f6c54ae6c19f7', '尼玛', 'offline'),
(10022, '/webroot/file/2019/11/193e937af8c8c647.jpg', 'dfd', '222', 'd9ae6f2629a48726ec9f6c54ae6c19f7', 'fdf ', 'online'),
(10023, '/webroot/file/2019/11/d6b4f4eb726c5aed.jpg', '广泛', '333', 'd9ae6f2629a48726ec9f6c54ae6c19f7', '3飞对规范v', 'offline'),
(10024, '/webroot/file/2019/11/db26b346c47522c4.jpg', 'gfgf', '555', 'd9ae6f2629a48726ec9f6c54ae6c19f7', 'gfgf', 'offline'),
(10025, '/webroot/file/2019/11/aa7424a7dc32d02d.jpg', 'dfdf ddfdfdfd', '666', 'd9ae6f2629a48726ec9f6c54ae6c19f7', 'fdfdfd', 'offline'),
(10026, '', 'gffgf453', '999', 'd9ae6f2629a48726ec9f6c54ae6c19f7', 'gfgff', 'offline'),
(10027, '/webroot/file/2019/11/33cf21df99802b36.jpg', '34fdgyb', '000', 'd9ae6f2629a48726ec9f6c54ae6c19f7', 'gfgf', 'offline'),
(10028, '/webroot/file/2019/11/442c9a93e110a715.jpg', 'fbfvbvb', '12', 'd9ae6f2629a48726ec9f6c54ae6c19f7', 'gfgf', 'offline');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chat_record`
--
ALTER TABLE `chat_record`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `friend`
--
ALTER TABLE `friend`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `friend_group`
--
ALTER TABLE `friend_group`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `group_member`
--
ALTER TABLE `group_member`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `offline_message`
--
ALTER TABLE `offline_message`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `system_message`
--
ALTER TABLE `system_message`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `chat_record`
--
ALTER TABLE `chat_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- 使用表AUTO_INCREMENT `friend`
--
ALTER TABLE `friend`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- 使用表AUTO_INCREMENT `friend_group`
--
ALTER TABLE `friend_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- 使用表AUTO_INCREMENT `group`
--
ALTER TABLE `group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10020;

--
-- 使用表AUTO_INCREMENT `group_member`
--
ALTER TABLE `group_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- 使用表AUTO_INCREMENT `offline_message`
--
ALTER TABLE `offline_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- 使用表AUTO_INCREMENT `system_message`
--
ALTER TABLE `system_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- 使用表AUTO_INCREMENT `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10029;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
