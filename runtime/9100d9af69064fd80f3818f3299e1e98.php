<?php /*a:1:{s:17:"./views/test.html";i:1573458784;}*/ ?>
<!-- //微聊消息上墙面板 -->
<div class="wc__chatMsg-panel flex1">
    <div class="wc__slimscroll2">
        <div class="chatMsg-cnt">
            <ul class="clearfix" id="J__chatMsgList">
                <li class="time"><span>2017年12月28日 晚上21:10</span></li>
                <li class="notice"><span>"<a href="#">Aster</a>"通过扫描"<a href="#">张小龙</a>"分享的二维码加入群聊</span></li>
                <li class="time"><span>2017年12月28日 晚上23:15</span></li>
                <li class="notice"><span>"<a href="#">雷军</a>"通过扫描"<a href="#">李彦宏</a>"分享的二维码加入群聊</span></li>
                <li class="notice"><span>当前群聊人数较多，已显示群成员昵称，同时为了信息安全，请注意聊天隐私</span></li>

                <li class="time"><span>2017年12月31日 晚上22:30</span></li>
                <!-- 别人-->
                <li class="others">
                    <a class="avatar" href="好友主页(详细资料).html"><img src="img/uimg/u__chat-img01.jpg" /></a>
                    <div class="content">
                        <p class="author">马云(老子天下第一)</p>
                        <div class="msg">
                            hello 各位女士、先生，欢迎大家来到达摩派，进群后记得修改备注哈~~ 名字+公司/职业/机构 <img class="face" src="img/emotion/face01/29.png"><img class="face" src="img/emotion/face01/71.png"><img class="face" src="img/emotion/face01/75.png">
                        </div>
                    </div>
                </li>
                <!--自己-->
                <li class="me">
                    <div class="content">
                        <p class="author">Nice奶思</p>
                        <div class="msg">
                            么么哒，马总发个红包呗！
                        </div>
                    </div>
                    <a class="avatar" href="好友主页(详细资料).html"><img src="img/uimg/u__chat-img14.jpg" /></a>
                </li>
                <li class="others">
                    <a class="avatar" href="好友主页(详细资料).html"><img src="img/uimg/u__chat-img02.jpg" /></a>
                    <div class="content">
                        <p class="author">MR（马蓉  ㈱）</p>
                        <div class="msg">
                            马总，晚上好哇，还木休息呢。我还在景区度假呢，棒棒！ <img class="face" src="img/emotion/face01/69.png">
                        </div>
                    </div>
                </li>
                <li class="others">
                    <a class="avatar" href="好友主页(详细资料).html"><img src="img/uimg/u__chat-img02.jpg" /></a>
                    <div class="content">
                        <p class="author">MR（马蓉  ㈱）</p>
                        <div class="msg picture">
                            <img class="img__pic" src="img/placeholder/wchat__img03.jpg" />
                        </div>
                    </div>
                </li>
                <li class="others">
                    <a class="avatar" href="好友主页(详细资料).html"><img src="img/uimg/u__chat-img12.jpg" /></a>
                    <div class="content">
                        <p class="author">Flowers（杨迪）</p>
                        <div class="msg">
                            哼，要红包。 <img class="face" src="img/emotion/face01/63.png">
                        </div>
                    </div>
                </li>
                <li class="time"><span>1月1日 早上02:00</span></li>
                <li class="others">
                    <a class="avatar" href="好友主页(详细资料).html"><img src="img/uimg/u__chat-img04.jpg" /></a>
                    <div class="content">
                        <p class="author">Xlong（张小龙）</p>
                        <div class="msg">
                            小程序后台新增推广功能，支持开发者添加与业务相关的自定义关键词!<br>
                            <a >https://mp.weixin.qq.com/cgi-bin/wx</a>
                        </div>
                    </div>
                </li>
                <li class="me">
                    <div class="content">
                        <p class="author">Nice奶思</p>
                        <div class="msg">
                            小龙哥好敬业哇，牛牛牛！<img class="face" src="img/emotion/face01/79.png">
                        </div>
                    </div>
                    <a class="avatar" href="好友主页(详细资料).html"><img src="img/uimg/u__chat-img14.jpg" /></a>
                </li>
                <li class="me">
                    <div class="content">
                        <p class="author">Nice奶思</p>
                        <div class="msg video">
                            <img class="img__video" src="img/placeholder/wchat__video02-poster.jpg" videoUrl="img/placeholder/wchat__video02-Y7qk5uVcNcFJIY8O4mKzDw.mp4" />
                        </div>
                    </div>
                    <a class="avatar" href="好友主页(详细资料).html">
                        <img src="img/uimg/u__chat-img14.jpg" />
                    </a>
                </li>
                <li class="time"><span>2月25日 早上09:48</span></li>
                <li class="others">
                    <a class="avatar" href="好友主页(详细资料).html"><img src="img/uimg/u__chat-img06.jpg" /></a>
                    <div class="content">
                        <p class="author">Robin（李彦宏）</p>
                        <div class="msg">
                            早上好，各位，这次人机交互线下活动的视频及PPT预计明天可以公开啦 <img class="face" src="img/emotion/face01/4.png">
                        </div>
                    </div>
                </li>
                <li class="others">
                    <a class="avatar" href="好友主页(详细资料).html"><img src="img/uimg/u__chat-img15.jpg" /></a>
                    <div class="content">
                        <p class="author">King（李嘉诚）</p>
                        <div class="msg">
                            这个不错，支持下~ <img class="face" src="img/emotion/face01/42.png">
                        </div>
                    </div>
                </li>
                <li class="time"><span>3月12日 下午14:28</span></li>
                <li class="others">
                    <a class="avatar" href="好友主页(详细资料).html"><img src="img/uimg/u__chat-img03.jpg" /></a>
                    <div class="content">
                        <p class="author">Jay（周杰伦）</p>
                        <div class="msg">
                            我的新专辑《告白气球》将于6.1上线，到时希望大家多多支持啦~ <img class="face" src="img/emotion/face01/66.png">
                        </div>
                    </div>
                </li>
                <li class="others">
                    <a class="avatar" href="好友主页(详细资料).html"><img src="img/uimg/u__chat-img11.jpg" /></a>
                    <div class="content">
                        <p class="author">Luci（王巧巧）</p>
                        <div class="msg picture">
                            <img class="img__pic" src="img/placeholder/wchat__img01.jpg" />
                        </div>
                    </div>
                </li>
                <li class="time"><span>"马云(老子天下第一)" 撤回了一条消息</span></li>
                <li class="others">
                    <a class="avatar" href="好友主页(详细资料).html"><img src="img/uimg/u__chat-img11.jpg" /></a>
                    <div class="content">
                        <p class="author">Luci（王巧巧）</p>
                        <div class="msg video">
                            <img class="img__video" src="img/placeholder/wchat__video01-poster.jpg" videoUrl="img/placeholder/wchat__video01-Y7qk5uVcNcFJIY8O4mKzDw.mp4" />
                        </div>
                    </div>
                </li>
                <li class="time"><span>"Luci（王巧巧）" 已被移出群聊</span></li>
                <li class="me">
                    <div class="content">
                        <p class="author">Nice奶思</p>
                        <div class="msg picture">
                            <img class="img__pic" src="img/placeholder/wchat__img02.jpg">
                        </div>
                    </div>
                    <a class="avatar" href="好友主页(详细资料).html"><img src="img/uimg/u__chat-img14.jpg" /></a>
                </li>
                <li class="me">
                    <div class="content">
                        <p class="author">Nice奶思</p>
                        <div class="msg">
                            北京新世纪饭店发放福利啦，免费领取VIP会员，大家快去参与吧。
                        </div>
                    </div>
                    <a class="avatar" href="好友主页(详细资料).html"><img src="img/uimg/u__chat-img14.jpg" /></a>
                </li>
            </ul>
        </div>
    </div>
</div>

